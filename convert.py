import math
import random
import cv2
import numpy as np
from alive_progress import alive_bar
from estimate import estimate

def mround(x, m):
    return m * round(x / m)

raw = cv2.imread("test.jpg")
rh, rw, _ = raw.shape
h = mround(rh / 2, 8)
w = mround(rw / 2, 8)
img = cv2.resize(raw, (w, h))

# BGR, like opencv
colors = [[183, 226, 234],
          [ 73,  48,   0],
          [ 40,  40, 214]]

def coord(x, y, n):
    return (x + math.floor(n / 8), y + (n % 8))

def superpixel(img, x, y):
    a = [0,0,0]
    for n in range(64):
        i, j = coord(x, y, n)
        a += img[j,i]
    cl = estimate(colors, a)
    l = list(range(64))
    random.shuffle(l)
    n = 0
    for c in range(3):
        while cl[c] > 0:
            i, j = coord(x, y, l[n])
            img[j,i] = colors[c]
            cl[c] -= 1
            n += 1

with alive_bar(int(h * w / 64)) as bar:
    bar.title = 'Round 1'
    bar.title_length = 7
    for x in range(int(w / 8)):
        for y in range(int(h / 8)):
            superpixel(img, x*8, y*8)
            bar()

cv2.imshow("img", img)
cv2.waitKey(0)

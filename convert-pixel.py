import cv2
import numpy as np
from alive_progress import alive_bar

img = cv2.imread("test.jpg")
print(img.shape)
h, w, _ = img.shape

# BGR, like opencv
colors = [[183, 226, 234],
          [ 73,  48,   0],
          [ 40,  40, 214]]

def pixel(img, x, y):
    mc = [0,0,0]
    md = 444
    for c in colors:
        d = np.linalg.norm(img[y,x]-c)
        if d < md:
            md = d
            mc = c
    return mc

with alive_bar(h * w) as bar:
    bar.title = 'Round 1'
    bar.title_length = 7
    for x in range(w):
        for y in range(h):
            img[y,x] = pixel(img, x, y)
            bar()

cv2.imshow("img", img)
cv2.waitKey(0)

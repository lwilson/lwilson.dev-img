import numpy as np

def estimate(c, a):
    # len(c) must = 3
    b = [0, 0, 0]
    d = 999999
    for i in range(65):
        for j in range(65 - i):
            t = [i, j, 64 - i - j]
            n = np.linalg.norm(a - np.matmul(np.transpose(c), t))
            if n < d:
                b = t
                d = n
    return b
